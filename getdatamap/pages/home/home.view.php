<!-- table -->
	<div class="row tall-row">
	    <div class="col-lg-12">
	        <h2>Danh sách dự án</h2>
	        <hr>
	    </div>
	</div>
        <div class="row">
            <div class="col-lg-12">
                <table class="table table-striped table-hover ">
                    <thead>
                        <tr>
                            <th>#</th>
                            <th>Tên dự án</th>
                            <th>Người tạo</th>
                            <th>Hành động</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php 
                             $stt=1;
                            $projects = json_decode($projects,true);
                            foreach ($projects['projects'] as $row) {
                        ?>
                        <tr>
                            <td><?=$stt?></td>
                            <td><?= $row['title']?></td>
                            <td ><?= $row["owner_email"]?></td>
                            <td><a href="?page=data&token=<?=$row["token"]?>" class="btn btn-primary"><i class="fas fa-eye" ></i> xem</a></td>
                        </tr>
                    <?php } ?>
                    </tbody>
                </table> 
            </div>
        </div>