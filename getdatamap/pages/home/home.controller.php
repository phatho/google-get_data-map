<?php 

//display
$params = http_build_query(array(
    "api_key" => "$api_key",
    "offset" => "0",
    "limit" => "20",
    "include_options" => "1"
));

$projects = file_get_contents(
    'https://www.parsehub.com/api/v2/projects?'.$params,
    false,
    stream_context_create(array(
        'http' => array(
            'method' => 'GET'
        )
    ))
);
