<!-- <div class="progress">
	    <div class="progress-bar progress-bar-warning" style="width: 60%"></div>
	</div> -->

<div class="tiendo " style="display: none;">
    <div class="progress active" >
    <div class="progress-bar progress-bar-success progress-bar-striped"  role="progressbar" aria-valuenow="40" aria-valuemin="0" aria-valuemax="100" id="progressbar" style="width: 0%">
        <span id="phantram" class=""></span>
    </div>
  </div>
</div>

    
        <div class="row">
            <div class="col-lg-12 text-right">
                 <a class="btn btn-warning  btn-lg " href="?page="> <i class="fas fa-long-arrow-alt-left" ></i> Quay lại</a>
                <button onclick="importShops()" class="btn btn-danger btn-lg"> <i class="fas fa-cloud-download-alt" ></i> Lấy data</button>
            </div>
            <div class="col-lg-12">
                <table class="table table-striped table-hover ">
                    <thead>
                        <tr>
                            <th>#</th>
                            <th>Tên </th>
                            <th>Địa chỉ</th>
                            <th>Vĩ độ</th>
                            <th>Kinh độ</th>
                            <th>Trạng thái</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php 
                             $stt=1;
                            foreach ($data as $row) {
                                if(empty($row['address'])) {
                                    $row['address'] = '';
                                }
                        ?>
                        <tr>
                            <td><?=$stt?></td>
                            <td><?= $row['name'] ?></td>
                            <td id = "address_<?=$stt?>"><?= $row["address"] ?></td>
                            <td id = "lat_<?=$stt?>"></td>
                            <td id = "lng_<?=$stt?>"></td>
                             <td id = "status_<?=$stt?>"> Chưa xử lý</td>
                        </tr>
                    <?php 
                    $stt ++;
                } ?>
                    </tbody>
                </table> 
            </div>
        </div>